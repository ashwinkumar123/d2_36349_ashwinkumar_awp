1. Mukesh Ambani

Mukesh Dhirubhai Ambani (born 19 April 1957) is an Indian billionaire business magnate, and the chairman, managing director, and largest shareholder of Reliance Industries Ltd. (RIL), a Fortune Global 500 company and India's most valuable company by market value.[5] He is currently the richest man in Asia with a net worth of US$80.5 billion and as of 24 July 2020 he is listed on Forbes as the 5th richest person in the world.[6][7]

2. Ratan tata

Ratan Naval Tata (born 28 December 1937) is an Indian industrialist, philanthropist, and a former chairman of Tata Sons. He was also chairman of Tata Group, from 1990 to 2012, and again, as interim chairman, from October 2016 through February 2017, and continues to head its charitable trusts.[3][4] He is the recipient of two of the highest civilian awards of India, the Padma Vibhushan (2008) and Padma Bhushan (2000).[5] He is well known for his business ethics and philanthropy.

3. Anand Mahindra

Anand Gopal Mahindra (born 1 May 1955) is an Indian billionaire businessman, and the chairman of Mahindra Group, a Mumbai-based business conglomerate.[4][5][6][7] The group operates in aerospace, agribusiness, aftermarket, automotive, components, construction equipment, defence, energy, farm equipment, finance and insurance, industrial equipment, information technology, leisure and hospitality, logistics, real estate and retail.[8] Mahindra is the grandson of Jagdish Chandra Mahindra, co-founder of Mahindra & Mahindra.

4. Elon Musk

Elon Reeve Musk FRS (/ˈiːlɒn/; born June 28, 1971) is an engineer, industrial designer, technology entrepreneur and philanthropist.[2][3][4][5] He is the founder, CEO, CTO and chief designer of SpaceX;[6] early investor,[7][note 1] CEO and product architect of Tesla, Inc.;[10][11] founder of The Boring Company;[12] co-founder of Neuralink; and co-founder and initial co-chairman of OpenAI.[13] He was elected a Fellow of the Royal Society (FRS) in 2018.[14][15] In December 2016, he was ranked 21st on the Forbes list of The World's Most Powerful People,[16] and was ranked joint-first on the Forbes list of the Most Innovative Leaders of 2019.[17] As of July 24, 2020 his net worth was estimated at $65 billion and he is listed by Forbes as the 11th-richest person in the world. [18][19][1] He is the longest tenured CEO of any automotive manufacturer globally.

5. Mark Zuckerberg

Mark Elliot Zuckerberg (/ˈzʌkərbɜːrɡ/; born May 14, 1984) is an American media magnate, internet entrepreneur, and philanthropist. He is known for co-founding Facebook, Inc. and serves as its chairman, chief executive officer, and controlling shareholder.[3][4] He also is a co-founder of the solar sail spacecraft development project Breakthrough Starshot and serves as one of its board members.

6. Steve Jobs

Steven Paul Jobs (/dʒɒbz/; February 24, 1955 – October 5, 2011) was an American business magnate, industrial designer, investor, and media proprietor. He was the chairman, chief executive officer (CEO), and co-founder of Apple Inc., the chairman and majority shareholder of Pixar, a member of The Walt Disney Company's board of directors following its acquisition of Pixar, and the founder, chairman, and CEO of NeXT. Jobs is widely recognized as a pioneer of the personal computer revolution of the 1970s and 1980s, along with Apple co-founder Steve Wozniak.

7. Jeff Bezoz

Jeffrey Preston Bezos (/ˈbeɪzoʊs/;[a][2] né Jorgensen; born January 12, 1964) is an American internet entrepreneur, industrialist, media proprietor, and investor. He is best known as the founder, CEO, and president of the multi-national technology company Amazon. The first centi-billionaire on the Forbes wealth index, Bezos has been the world's richest person since 2017 and was named the "richest man in modern history" after his net worth increased to $150 billion in July 2018.

8. Jack Ma

Jack Ma, or Ma Yun[a] (Chinese: 马云; [mà y̌n]), is a Chinese business magnate, investor and philanthropist. He is the co-founder and former executive chairman of Alibaba Group, a multinational technology conglomerate. Ma is a strong proponent of an open and market-driven economy.

9. Bill gates

William Henry Gates III (born October 28, 1955) is an American business magnate, software developer, investor, and philanthropist. He is best known as the co-founder of Microsoft Corporation.[2][3] During his career at Microsoft, Gates held the positions of chairman, chief executive officer (CEO), president and chief software architect, while also being the largest individual shareholder until May 2014. He is one of the best-known entrepreneurs and pioneers of the microcomputer revolution of the 1970s and 1980s.
